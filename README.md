# KernVecFunctions

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/KernVecFunctions.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/KernVecFunctions.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/KernVecFunctions.jl/-/pipelines)

This is an experimental package which defines `KernVecFunction` types.  These are functions which
allow users to define functions over statically sized vectors (using StaticArrays.jl) and easily run
GPU kernels which apply these to each column of a matrix.

The basic forward-mode implementation is quite simple, thanks to KernelAbstractions.jl, however
defining efficient AD over these is yet to be completed and will likely be horrendous.

Better documentation will be found here if this package turns out to be useful.
