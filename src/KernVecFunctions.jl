module KernVecFunctions

using LinearAlgebra, StaticArrays
using KernelAbstractions
using ForwardDiff
using ChainRulesCore

#====================================================================================================
Well... this has gone surprisingly smoothly... next onto the dreaded rrules...       
====================================================================================================#


abstract type KernVecFunction{odim,argdims} end


struct KernVecFunc{odim,argdims,F} <: KernVecFunction{odim,argdims}
    func::F
end

KernVecFunc{odim,argdims}(𝒻) where {odim,argdims} = KernVecFunc{odim,argdims,typeof(𝒻)}(𝒻)


@generated function _argvecs(::Val{argdims}, j::Integer, xsv::AbstractMatrix...) where {argdims}
    xsym = map(j -> gensym(), 1:length(argdims))
    lines = map(1:length(argdims)) do i
        args = map(k -> :(xsv[$i][$k,j]), 1:argdims[i])
        :($(xsym[i]) = SVector{$(argdims[i])}($(args...)))
    end
    last = :(tuple($(xsym...)))
    push!(lines, last)
    Expr(:block, lines...)
end

@kernel function kernvecfunc_kernel!(𝒻, ::Val{odim}, ::Val{argdims},
            y::AbstractMatrix, xs::AbstractMatrix...
        ) where {odim,argdims}
    j = @index(Global)
    xsv = _argvecs(Val(argdims), j, xs...)
    o = 𝒻(xsv...)
    @inbounds for i ∈ 1:odim
        y[i,j] = o[i]
    end
    nothing
end

default_workgroup_size(::Backend) = 1024
default_workgroup_size(::GPU) = 256

function apply!(𝒻, back::Backend, vo::Val{odim}, varg::Val{argdims}, l::Integer, y::AbstractMatrix,
        xs::AbstractMatrix...;
        workgroup_size::Integer=default_workgroup_size(back),
    ) where {odim,argdims}
    kernel! = kernvecfunc_kernel!(back, workgroup_size)
    kernel!(𝒻, vo, varg, y, xs...; ndrange=(l,))
    y
end

function _checkargs(back::Backend, ::Val{odim}, ::Val{argdims}, l::Integer, y::AbstractMatrix,
        xs::AbstractMatrix...
    ) where {odim,argdims}
    if size(y,1) ≠ odim
        throw(ArgumentError("incorrect output size"))
    end
    for (j,adim) ∈ enumerate(argdims)
        if size(xs[j],1) ≠ adim
            throw(ArgumentError("invalid argument matrix size for KernVecFunc"))
        end
    end
    if !all(x -> size(x,2) == l, xs)
        throw(ArgumentError("got matrices of inconsistent numbers of columns"))
    end
    if !all(x -> get_backend(x) == back, xs)
        throw(ArgumentError("got matrices with inconsistent backends"))
    end
end

function apply!(𝒻::KernVecFunction{odim,argdims}, y::AbstractMatrix, xs::AbstractMatrix...) where {odim,argdims}
    l = size(xs[1],2)
    back = get_backend(y)
    _checkargs(back, Val(odim), Val(argdims), l, y, xs...)
    apply!(𝒻.func, back, Val(odim), Val(argdims), l, y, xs...)
end

function (𝒻::KernVecFunction{odim,argdims})(xs::AbstractMatrix...) where {odim,argdims}
    y = similar(xs[1], odim, size(xs[1],2))
    apply!(𝒻, y, xs...)
end


struct KernVecFuncWithGrad{argdims,F,Gs<:Tuple} <: KernVecFunction{1,argdims}
    func::F
    grads::Gs
end

#TODO: this is a big pain in the ass
# need to return functions of all vars that are gradient of selected var
# see examples in scrap
#
# also probably going to be really hard to get this to always work on GPU...

function defualt_grads(𝒻, ::Val{argdims}) where {argdims}
    error("I haven't implmented this yet because it seems like a clusterfuck")
end

#FUCK: will have to do jacobians if you want this to work reliably
# no, I'm not sure how the shape will work, will we need rank-3?

function KernVecFuncWithGrad{argdims}(𝒻, grads=default_grads(𝒻, Val(argdims))) where {argdims}
    KernVecFuncWithGrad{argdims,typeof(𝒻),typeof(grads)}(𝒻, grads)
end



include("rules.jl")


export KernVecFunc, KernVecFuncWithGrad


end
