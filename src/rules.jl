

function _inner_kernvec_grad(dy, ::KernVecFuncWithGrad{argdims},
        grad, ::Val{i}, xs::AbstractMatrix...
    ) where {i,argdims}
    o = similar(xs[i])
    apply!(grad, get_backend(o), Val(argdims[i]), Val(argdims), size(o,2), o, xs...)
    #WARN: are you sure about this?
    o .* dy
end


function ChainRulesCore.rrule(cfg::RuleConfig{>:HasReverseMode},
        𝒻::KernVecFuncWithGrad{argdims}, xs::AbstractMatrix...
    ) where {argdims}
    y = 𝒻(xs...)
    function kernvecfunc_pullback(dy_raw)
        dy = unthunk(dy_raw)
        grads = ntuple(Val(length(xs))) do i
            @thunk _inner_kernvec_grad(dy, 𝒻, 𝒻.grads[i], Val(i), xs...)
        end
        (NoTangent(), grads...)
    end
    kernvecfunc_pullback(::AbstractZero) = (NoTangent(), ntuple(Returns(ZeroTangent()), Val(length(xs)))...)
    (y, kernvecfunc_pullback)
end


