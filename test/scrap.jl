using LinearAlgebra, BenchmarkTools, KernelAbstractions, StaticArrays
using ChainRulesCore, Zygote, ForwardDiff
using CUDA
using KernVecFunctions

using KernVecFunctions: KernVecFunc, KernVecFuncWithGrad, apply!

const TEST_LENGTH = 10^4

const G = 1.0f0
const m = 1.0f0
const k = 1.0f0

refinner(q1::AbstractMatrix, q2::AbstractMatrix) = sum(q1 .* q2, dims=1)

function refH!(h, q, p)
    h .= refinner(p, p)/(2*m) .+ (k/2)*refinner(q, q)
end

refH(q, p) = refinner(p, p)/(2*m) .+ (k/2)*refinner(q, q)


function invmetric(x::AbstractVector)
    A = 1 - 2*G*m/x[1]
    @SMatrix eltype(x)[
        1/A 0 0 0
        0 x[1]^(-2) 0 0
        0 0 (x[1]*sin(x[2]))^(-2) 0
        0 0 0 -(1/A)
    ]
end


H(x, p) = p' * invmetric(x) * p ./ 2

ho(q, p) = (p⋅p)/(2*m) + (k/2)*(q⋅q)


src1() = quote
    x1 = randn(2,4) |> cu
    x2 = randn(3,4) |> cu
    y = fill(NaN, 1, 4) |> cu

    f = KernVecFunc{1,(2,3)}((a, b) -> sum(a) + sum(b))

    f(x1, x2)
end

similar_ones(x::AbstractArray) = map(ξ -> one(eltype(x)), x)

src2() = quote
    cfg = Zygote.ZygoteRuleConfig()

    x1 = randn(2,4) |> cu
    x2 = randn(3,4) |> cu

    𝒻 = (a, b) -> 2 .* sum(a) + sum(b)
    grad1 = (a, b) -> 2 .* similar_ones(a)
    #grad1 = (a, b) -> ForwardDiff.gradient(a) do α
    #    𝒻(α, b)
    #end
    grad2 = (a, b) -> similar_ones(b)
    #grad2 = (a, b) -> ForwardDiff.gradient(b) do β
    #    𝒻(a, β)        
    #end

    f = KernVecFuncWithGrad{(2,3)}(𝒻, (grad1, grad2))

    (grad,) = Zygote.gradient(x2) do ξ
        sum(f(x1, ξ))
    end
end

src3() = quote
    x1 = randn(2)
    x2 = randn(3)

    𝒻 = (a, b) -> a

    fg = ForwardDiff.jacobian(ξ -> 𝒻(x1,ξ), x2)
end

