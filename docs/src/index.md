```@meta
CurrentModule = KernVecFunctions
```

# KernVecFunctions

Documentation for [KernVecFunctions](https://gitlab.com/ExpandingMan/KernVecFunctions.jl).

```@index
```

```@autodocs
Modules = [KernVecFunctions]
```
