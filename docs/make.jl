using KernVecFunctions
using Documenter

DocMeta.setdocmeta!(KernVecFunctions, :DocTestSetup, :(using KernVecFunctions); recursive=true)

makedocs(;
    modules=[KernVecFunctions],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    sitename="KernVecFunctions.jl",
    format=Documenter.HTML(;
        canonical="https://ExpandingMan.gitlab.io/KernVecFunctions.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
